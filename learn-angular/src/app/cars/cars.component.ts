import { Component } from '@angular/core';

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent {

  constructor() { }
 
  carName = '';
  addCarStatus = false;
  cars = [];
  dates = [
    new Date(2011, 3, 9),
    new Date(2016, 1, 13),
    new Date(2014, 7, 12),
    new Date(2013, 5, 29)
  ];

  addText() {
    this.addCarStatus = true;
    this.cars.push(this.carName);
    this.carName = '';
  }

  delText() {
    this.cars.pop();
    console.log(this.cars)
  }


}
